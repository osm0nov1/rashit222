//
//  Dynamick.swift
//  rashit222
//
//  Created by Rashit Osmonov on 21/6/22.
//

import Foundation
class Dynamik<T> {
    typealias Listener = (T) -> Void
    private var listener: Listener?
    
    func bind(_ listener: Listener?) {
        self.listener = listener
    }
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init (_ v:T){
        value = v
    }
}
