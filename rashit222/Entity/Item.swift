//
//  Item.swift
//  rashit222
//
//  Created by Rashit Osmonov on 21/6/22.
//

import Foundation

struct Item {
    let name: String
    let number: String
}
