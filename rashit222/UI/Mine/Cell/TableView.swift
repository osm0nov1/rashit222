//
//  TableView.swift
//  rashit222
//
//  Created by Rashit Osmonov on 21/6/22.
//

import Foundation
import SnapKit
class TableView: UITableViewCell {
    
    private lazy var name: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .medium)
        return view
    }()
    private lazy var number: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 10, weight: .medium)
        return view
    }()
    override func layoutSubviews() {
        
        addSubview(name)
        name.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(10)
        }
        addSubview(number)
        number.snp.makeConstraints { make in
            make.top.equalTo(name.snp.bottom).offset(3)
            make.left.equalToSuperview().offset(10)
        }
    }
    func fill(item: Item){
        self.name.text = "Name: \(item.name)"
        self.number.text = "Number \(item.number)"
    }
}

