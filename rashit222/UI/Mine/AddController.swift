//
//  AddController.swift
//  rashit222
//
//  Created by Rashit Osmonov on 21/6/22.
//

import Foundation
import SnapKit
class AddController: UIViewController {
    
    private lazy var name: UITextField = {
        let view = UITextField()
        view.layer.cornerRadius = 10
        view.placeholder = "Name"
        view.backgroundColor = .white
        view.borderStyle = .roundedRect
        return view
    }()
    private lazy var number: UITextField = {
        let view = UITextField()
        view.layer.cornerRadius = 10
        view.placeholder = "Number"
        view.backgroundColor = .white
        view.borderStyle = .roundedRect
        view.layer.sublayerTransform = CATransform3DMakeTranslation(0, 10, 0);
        return view
    }()
    private lazy var save: UIButton = {
        let view = UIButton()
        view.setTitle("Save", for: .normal)
        view.backgroundColor = .blue
        view.addTarget(self, action: #selector(saveButton(view:)), for: .touchUpInside)
        return view
    }()
    
    @objc func saveButton(view: UIButton){
        ViewModel.shered.getData(model: Item(name: name.text ?? "", number: number.text ?? ""))
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        view.addSubview(name)
        name.snp.makeConstraints { make in
//            make.left.equalToSuperview().offset(10)
            make.centerX.equalToSuperview().offset(-140)
            make.centerY.equalToSuperview().offset(-100)

        }
        
        view.addSubview(number)
        number.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-250)
            make.top.equalTo(name.snp.bottom).offset(25)
        }
        view.addSubview(save)
        save.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.width.height.equalTo(60)
            make.centerX.equalToSuperview()
            make.leading.trailing.equalToSuperview()
        }
        
    }
}
