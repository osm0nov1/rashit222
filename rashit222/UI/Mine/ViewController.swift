//
//  ViewController.swift
//  rashit222
//
//  Created by Rashit Osmonov on 21/6/22.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    
    private lazy var mainTable: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        return view
    }()
    private lazy var uiButton: UIButton = {
        let view = UIButton()
        view.setTitle("Add", for: .normal)
        view.backgroundColor = .blue
        view.tintColor = .black
        view.addTarget(self, action: #selector(addButton(view:)), for: .touchUpInside)
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        setUpView()
        setUpSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        mainTable.reloadData()
    }
    private func bindViewModel() {
    }
    private func setUpView() {
        
    }
    
    @objc func addButton(view: UIButton) {
        navigationController?.pushViewController(AddController(), animated: true)
    }
    private func setUpSubviews() {
        view.addSubview(mainTable)
        mainTable.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-50)
        }
        view.addSubview(uiButton)
        uiButton.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.right.equalTo(view.safeArea.right).offset(-10)
            make.height.width.equalTo(45)
            
        }
    }
}
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  ViewModel.shered.saveContack.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TableView()
        let model = ViewModel.shered.saveContack[indexPath.row]
        cell.fill(item: model)
        return cell
    }
}


