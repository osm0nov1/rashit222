//
//  xtension.swift
//  rashit222
//
//  Created by Rashit Osmonov on 22/6/22.
//

import Foundation
import SnapKit
extension UIView {
    
    var safeArea: ConstraintBasicAttributesDSL {
        #if swift(>=3.2)
            if #available(iOS 11.0, *) {
                return self.safeAreaLayoutGuide.snp
            }
            return self.snp
        #else
            return self.snp
        #endif
    }
}
